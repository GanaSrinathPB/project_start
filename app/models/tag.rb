class Tag < ApplicationRecord
  has_many :taggings
  has_many :posts, through: :taggings, dependent: :destroy
    validates :tagname, :uniqueness => {:case_sensitive => false},if: :tag_required

  def tag_required
    tagname.present?
  end
end
