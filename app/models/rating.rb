class Rating < ApplicationRecord
  belongs_to :post
  validates_presence_of :rate
   validates_uniqueness_of :post_id, scope: :user_id
end
