class UserCommentRating < ApplicationRecord
  belongs_to :user
  belongs_to :comment
#=====================================Validation========================================================================
  validates :rate, presence: true
  validates_uniqueness_of :comment_id, scope: :user_id

end
