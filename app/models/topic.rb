class Topic < ApplicationRecord

  #======================================Associations===================================================================
  belongs_to :user
  has_many :posts , dependent: :destroy
 #========================================Validations===================================================================
  validates_presence_of :name
  validates :name, :uniqueness => {:case_sensitive => false}
  validates :name, length: {maximum: 20}

end
