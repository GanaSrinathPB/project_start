class Comment < ApplicationRecord
  belongs_to :post, counter_cache: true
  belongs_to :user
  has_many :user_comment_ratings #the associated model with user and comment
  has_many :users, through: :user_comment_ratings, dependent: :destroy
#===============================validATION==============================================================================
  validates_presence_of :name, :body
end
