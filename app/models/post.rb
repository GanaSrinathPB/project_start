class Post < ApplicationRecord

  #====================== Relations ====================================================================================

  belongs_to :topic
  belongs_to :user
  has_many :comments, dependent: :destroy
  has_many :taggings  #the associated model with tag and post
  has_many :tags, through: :taggings, dependent: :destroy
  has_many :ratings, dependent: :destroy
  has_and_belongs_to_many :users
  has_attached_file :image,default_url: ':style/missing_img.png', styles: {small: "64*64#", med: "100*100>", large: "200*200>"}
  #====================== Validations ==================================================================================
  validates_presence_of :name, :description
  validates :name, length: {maximum: 20}
  validates :name, :uniqueness => {:case_sensitive => false}
  validates :description, length: {minimum: 25}
  validates_attachment_content_type :image, content_type:/\Aimage\/.*\z/, message: "Please Upload valid images"
  #===============================Tagging===============================================================================

  accepts_nested_attributes_for :tags, allow_destroy: true, reject_if: proc { |attributes| attributes['tagname'].blank? }
#=====================================Scope for date====================================================================
  scope :search_date,->(start_date, end_date) {where(created_at: start_date..end_date)}
end






