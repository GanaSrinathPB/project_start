class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, uniqueness: true
  PASSWORD_FORMAT = /\A
  (?=.*\d)           # Must contain a digit
  (?=.*[a-z])        # Must contain a lower case character
  (?=.*[A-Z])        # Must contain an upper case character
  (?=.*[[:^alnum:]]) # Must contain a symbol
/x

  validates :password,
            presence: true,
            length: {in: Devise.password_length},
            format: {with: PASSWORD_FORMAT,message: "Must contain a digit,a lower case character,an
                   upper case character,a symbol"},
            confirmation: true
# #===================association=========================================================================================
  has_many :comments, dependent: :destroy
  has_many :topics, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :user_comment_ratings #the associated model with user and comment
  has_many :rate_comments, through: :user_comment_ratings, source: :comment, dependent: :destroy
  has_and_belongs_to_many :post_reads, class_name: 'Post', join_table: :posts_users


end
