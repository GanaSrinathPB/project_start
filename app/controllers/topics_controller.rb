class TopicsController < ApplicationController
  before_action :set_topic, except: [:index, :new, :create,:devise]
  before_action :topics_all, only: [:index, :create, :update]
  load_and_authorize_resource :only => [:edit, :update, :destroy]
  # GET /topics
  # GET /topics.json
  # sidekiq bundle exec sidekiq -q default -q mailers
  def index
  end

  def show

  end

  # GET /topics/new
  def new
    @topic = Topic.new
  end

  # GET /topics/1/edit
  def edit
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new(topic_params)
    respond_to do |format|
      if @topic.save
        # TopicMailer.delay.welcome_topic(current_user,@topic)
        EmailSenderWorker.perform_async(current_user.id, @topic.id)
        # TopicMailer.with(user: current_user,topic: @topic).welcome_topic.deliver_later
        format.html{redirect_to topics_path, notice: "Topic has been created"}
        format.js {render :create}
        format.json {render :show, status: :created, location: @topic}

      else
        format.html{render :new, notice: "Topic not been created"}
        format.js {render :create}
        format.json {render json: @topic.errors, status: :unprocessable_entity}

      end
    end
  end
  def devise
    # if request.env['HTTP_USER_AGENT'].downcase.match(/mac/i)
    #   redirect_to 'https://itunes.apple.com/us/app/apple-store/id375380948?mt=8'
    # elsif request.env['HTTP_USER_AGENT'].downcase.match(/windows/i)
    #   redirect_to 'https://www.microsoft.com/en-in/store/apps/windows'
    # elsif request.env['HTTP_USER_AGENT'].downcase.match(/android/i)
    #   redirect_to 'https://play.google.com/store?hl=en'
    # else
    #   redirect_to root_path
    # end
# p request.env['HTTP_USER_AGENT']
p  request.user_agent.downcase
     redirect_to case request.user_agent.downcase
                when /mac/i     then 'https://itunes.apple.com/us/app/apple-store/id375380948?mt=8'
                when /windows/i then 'https://www.microsoft.com/en-in/store/apps/windows'
                when /android/i then 'https://play.google.com/store?hl=en'
                else
                  root_path
                end
  end
  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    respond_to do |format|
      if @topic.update(topic_params)
        format.html {redirect_to topics_path, notice: "Topic was successfully updated"}
        format.js {render :create}
        format.json {render :show, status: :ok, location: @topic}
      else
        format.html {redirect_to topics_path, notice: "Topic not been updated"}
        format.js {render :create}
        format.json {render json: @topic.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic.destroy
    respond_to do |format|
      format.html {redirect_to topics_url, alert: 'Topic was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_topic
    @topic = Topic.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The Topic you were looking for could not be found."
    redirect_to topics_path

  end

  def topics_all
    @topics = Topic.order(created_at: :desc).includes(:user).paginate(page: params[:page], per_page: 8)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def topic_params
    params.require(:topic).permit(:name).merge(user_id: current_user.id)
  end
end
