class RatingsController < ApplicationController
  before_action :set_topic
  before_action :set_post
  before_action :set_rating, only: [:show, :edit, :update, :destroy]

  def index
    @ratings = @post.ratings
  end

  def new
    @rating = @post.ratings.new
  end

  def show
  end

  def edit
  end

  def create
    @rating = @post.ratings.new(rating_params)
    # respond_to do |format|
    if @rating.save
      # format.html{redirect_to topic_post_path(@topic, @post), notice:"rating has been done"}
      flash[:notice] = "rating has been done"
      redirect_to topic_post_path(@topic, @post)
    else
      # format.html{render :new, alert:"You have already rated the Post"}
        flash[:alert] = "You have already rated the Post"
        redirect_to topic_post_path(@topic, @post)
    end
      # end
  end

  def update
    if @rating.update(rating_params)
      flash[:notice] = "Ratting has been updated"
      redirect_to topic_post_ratings_path(@topic, @post, @rating)
    else
      flash[:alert] = "You have already rated the Post"
      redirect_to topic_post_ratings_path(@topic, @post, @rating)
    end
  end

  def destroy
    @rating.destroy
    respond_to do |format|
      format.html {redirect_to topic_post_rating_path, notice: 'Rating was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  def set_post
    @post = @topic.posts.find(params[:post_id])
  end

  def set_rating
    @rating = @post.ratings.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The Rating you were looking for could not be found."
    redirect_to topic_post_path(@topic, @post)
  end

  def rating_params
    params.fetch(:rating,{}).permit(:rate).merge(user_id: current_user.id)
  end

end
