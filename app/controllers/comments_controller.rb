class CommentsController < ApplicationController
  before_action :set_topic
  before_action :set_post
  before_action :set_comment, except: [:index, :create, :new]
  load_and_authorize_resource :topic
  load_and_authorize_resource :post
  load_and_authorize_resource :comment, through: [:topic, :post], only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = @post.comments.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = @post.comments.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = @post.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.html {redirect_to topic_post_path(@topic, @post), notice: 'Comment was successfully created.'}

      else
        format.html {render :new, alert: "Comment has not been created"}
        format.js {render :create}
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html {redirect_to topic_post_comments_path(@topic, @post, @comment), notice: 'Comment was successfully updated.'}
        format.json {render :show, status: :ok, location: @comment}
      else
        format.html {render :edit}
        format.json {render json: @comment.errors, status: :unprocessable_entity}
      end
    end
  end

  def comment_rate_show
    @rate_comment = @comment.user_comment_ratings.includes(:user)
  end

  def user_comment_rate
     @comment_ratings = @comment.user_comment_ratings.create(comment_rate_params)
    # p @comment_ratings.errors.details
    if @comment_ratings.save
      flash[:notice] = "successfully updated comment's rating by user"
    else
      flash[:alert] = "You have already rated the comment"
    end
    redirect_to topic_post_path(@topic, @post)
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html {redirect_to topic_post_path(@topic, @post), alert: 'Comment was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  def set_post
    @post = @topic.posts.find(params[:post_id])
  end

  def set_comment
    @comment = @post.comments.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The Record you were looking for could not be found."
    redirect_to topic_post_path(@topic,@post)
  end


  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:name, :body).merge(user_id: current_user.id)
  end

  def comment_rate_params
    params.fetch(:user_comment_rating,{}).permit(:rate).merge(user_id: current_user.id)
  end
end
