class PostsController < ApplicationController
  before_action :set_topic, except: [:index]
  before_action :set_post, only: [:edit, :update, :destroy, :post_user_status, :show]
  load_and_authorize_resource :topic
  load_and_authorize_resource :post, through: :topic, only: [:show, :edit, :update, :destroy]

  def index
    params[:start_date] = ((params[:start_date].present?) ? params[:start_date] : Date.today - 7)
    params[:end_date] = ((params[:end_date].present?) ? params[:end_date] : Date.today)
    if params[:start_date] >= params[:end_date]
      redirect_back(fallback_location: root_path)
      flash[:alert] = "date is out of range"
    end
    @posts = ((params[:topic_id].present?) ? @topic.posts : Post.all)
    @posts = @posts.search_date(params[:start_date], params[:end_date])

    if @posts.present?
      @posts = @posts.includes(:user).paginate(page: params[:page], per_page: 6).order(created_at: :desc)
    end
  end

  def show

    @ratecounts = Hash.new
    @ratecounts = {1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0}
    #raings by group_by
    @rating_count_groupby = @post.ratings.group_by(&:rate)
    @result1 = @ratecounts.merge(@rating_count_groupby) {|key, val1, val2| val1 + val2.count}
    #rating by group
    # @rating_count_group = @post.ratings.group(:rate).count
    # @result = @ratecounts.merge(@rating_count_group) {|key, val1, val2| val1 + val2}
    @rate_avg = @post.ratings.average(:rate)
    @comments = @post.comments.includes(:user)
  end

  def new
    @post = @topic.posts.new
  end

  def edit
  end

  def create
    @post = @topic.posts.new(post_params)
    respond_to do |format|
      if @post.save
        @posts = @topic.posts.paginate(page: params[:page], per_page: 6).order(created_at: :desc)
        format.html {redirect_to topic_posts_path, notice: "Post has been created"}
        format.js {render :create}
        format.json {render :show, status: :created, location: @post}
      else
        format.html {render :new, notice: "Post not been created"}
        format.js {render :create}
        format.json {render json: @post.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        @posts = @topic.posts.order(updated_at: :desc).paginate(page: params[:page], per_page: 6)
        format.html {redirect_to topic_posts_path, notice: "Post has been updated"}
        format.js {render :create}
        format.json {render :show, status: :created, location: @post}
      else
        format.html {render :edit, notice: "Post not been created"}
        format.js {render :create}
        format.json {render json: @post.errors, status: :unprocessable_entity}
      end
    end
  end

  def post_user_status
    unless @post.users.include?(current_user)
      @post.users << current_user
      head :no_content
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html {redirect_to topic_posts_path, alert: 'Post was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  def set_topic
    @topic = Topic.find(params[:topic_id])
  end

  def set_post
    @post = @topic.posts.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:alert] = "The Post you were looking for could not be found."
    redirect_to topic_posts_path(@topic)
  end

  def post_params
    params.require(:post).permit(:name, :description, :image, :tag_ids => [], tags_attributes: [:tagname]).merge(user_id: current_user.id)
  end

end
