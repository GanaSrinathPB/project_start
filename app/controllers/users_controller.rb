class UsersController < ApplicationController
  # skip_before_action :authenticate_user!

  def index
    authenticate_or_request_with_http_basic do |username, password|
      resource = User.find_by_email(username)
      if resource.valid_password?(password)
        render json: resource.to_json
      else
        render(json: 'Invalid credentials'.to_json, status: 401)
      end
    end
  end

end
#request_http_basic_authentication
# authenticate_or_request_with_http_basic