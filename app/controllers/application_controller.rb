class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token , if: :json_request?
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, :alert => exception.message
  end
  private
  def json_request?
    request.format.json?
  end

end
