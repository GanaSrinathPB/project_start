class TopicMailer < ApplicationMailer
  default from: 'srigana1997@gmail.com'

  def welcome_topic(user,topic)
    @topic=Topic.find_by(id: topic)
    @user=User.find_by(id: user)
    @url  = 'http://localhost:3000/topics'
    mail(to: @user.email, subject: 'Welcome to My Awesome Site')
  end
end
