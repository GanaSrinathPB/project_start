class ApplicationMailer < ActionMailer::Base
  default from: 'srigana1997@gmail.com'
  layout 'mailer'
end
