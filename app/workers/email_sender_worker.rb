class EmailSenderWorker
  include Sidekiq::Worker

  def perform(user,topic)
    TopicMailer.welcome_topic(user,topic).deliver_now
  end
end
