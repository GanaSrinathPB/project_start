require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @post = posts(:one)
  end

  test "should get index" do
    get posts_url
    assert_response :success
  end

  test "should get show" do
    get post_url(@post)
    assert_response :success
  end

  test "should get new" do
    get new_post_url
    assert_response :success
  end

  test "should get edit" do
    get edit_post_url(@post)
    assert_response :success
  end

  test "should create post" do
      assert_difference('Post.count') do
      post posts_url, params: { post: { name: @post.name, description: @post.description } }
    end
    assert_redirected_to post_url(Post.last)
  end

  test "should update Post" do
    patch post_url(@post), params: { post: { name: @post.name, description: @post.description } }
    assert_redirected_to post_url(@post)
  end

  test "should destroy post" do
        assert_difference('Post.count', -1) do
      delete post_url(@post)
    end
    assert_redirected_to post_url
  end

end
