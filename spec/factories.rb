FactoryBot.define do
  factory :user_comment_rating do
    user_id { 1 }
    comment_id { 1 }
    rate { 1 }
  end

  factory :user do
    email {'pbgsrinath@gmail.com'}
    password {'srinath'}
  end
  factory :topic do
    name {'students'}
    user_id {1}
  end
  factory :post do
    sequence(:name) {|n| "post#{n}"}
    # name {Faker::Post.name}
    description {'best 123654789654123654789 one'}
    image { File.new("#{Rails.root}/spec/support/fixtures/images/large/rails.jpg") }
  end
  factory :comment do
    name {'pbgsrinath@gmail.com'}
    body {'good one'}
  end
  factory :tag do
    tagname {'happy'}
  end
  factory :rating do
    rate {5}
  end
end