require 'rails_helper'
RSpec.describe EmailSenderWorker, type: :worker do
  describe "worked add sidekiq" do
    before do
      @user = User.create(email: "pbgsrinath@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
      @topic = @user.topics.create(name: "anything")
    end
    it 'send email to sidekiq' do
      expect{
        EmailSenderWorker.perform_async(@user.id,@topic.id)
      }.to change( EmailSenderWorker.jobs, :size ).by(1)

    end

  end
end
