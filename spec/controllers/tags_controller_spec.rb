require 'rails_helper'
RSpec.describe TagsController, type: :controller do

  before {
    @user = User.create(email: "email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    sign_in @user
    @user2 = User.create(email: "2email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    @topic = @user.topics.create(name: "anything")
    @post = @topic.posts.create(name: "gana", description: "123456789012345678901234567890", user_id: @user.id, created_at: Date.yesterday)
    @tag = @post.tags.create(tagname: "tag_name")
  }

  describe "GET #index" do
    it "returns a success response with post id" do
      get :index
      expect(response).to render_template("index")
      expect(response.request.path_info).to eql("/tags")
      expect(Tag.count).to eq(1)
      expect(response).to be_successful
    end
    it 'invalid sign in' do
      sign_out @user
      get :index
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
  end
  describe "GET #show" do
    it "return the tag posts" do
      get :show, params: {id:@tag.to_param}
      expect(assigns[:tag_post].first.name).to eq("gana")
      expect(assigns[:tag_post].first.description).to eq("123456789012345678901234567890")
      expect(assigns[:tag_post].size).to eq 1
      expect(response).to be_successful
    end
    it 'invalid sign in' do
      sign_out @user
      get :show, params: {id:@tag.to_param}
        expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
  end
  describe 'GET #new' do
    it 'returns success ' do
      get :new
      expect(assigns(:tag)).to be_a_new(Tag)
      expect(response).to be_successful
    end
  end
  describe 'get #create ' do
    it 'creates a new tag' do
      expect do
        post :create, params: {tag: {tagname: 'anything'}}
      end.to change(Tag, :count).by(1)
      expect(flash[:notice]).to eq 'Tag was successfully created.'
    end
    it 'creates a same  tag' do
      expect do
        post :create, params: {tag: {tagname: 'tag_name'}, post_id:@post.id}
      end.to change(Tag, :count).by(0)
      expect(assigns(:tag).errors[:tagname][0]).to eq("has already been taken")
    end
  end
  describe 'GET #edit' do
    it 'returns success ' do
      get :edit, params: {id: @tag.to_param}
      expect(assigns(:tag).tagname).to eq("tag_name")
      expect(response).to be_successful
    end
  end
  describe 'get #update ' do
    it 'update new tag' do
       patch :update, params: {id: @tag.to_param, tag: {tagname: 'update_anything'}}
      expect(assigns(:tag).tagname).to eq("update_anything")
      expect(flash[:notice]).to eq 'Tag was successfully updated.'
    end
  end
  describe "DELETE #destroy" do
    it "it destroy the requested topic" do
      delete :destroy, params: {id: @tag.to_param}
      expect(Tag.count).to eq(0)
      expect(flash[:alert]).to eq "Tag was successfully destroyed."
    end

  end
end