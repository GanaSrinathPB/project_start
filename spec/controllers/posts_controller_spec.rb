require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  before do
    @user = User.create(email: "email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    sign_in @user
    @user2 = User.create(email: "2email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    @topic = @user.topics.create(name: "anything")
    @post = @topic.posts.create(name: "gana", description: "123456789012345678901234567890", user_id: @user.id, created_at: Date.yesterday)
    @tag = @post.tags.create(tagname: "wow")
  end

  describe "GET #index" do
    context "with html request" do
      it "returns the successive responce without topic id" do
        get :index
        expect(response).to render_template("index")
        expect(response.request.path_info).to eql("/posts")
        expect(Post.count).to eq(1)
        expect(response).to be_successful
      end
      it "returns a success response with topic id" do
        get :index, params: {id: @post.to_param, topic_id: @topic.id}
        expect(response).to render_template("index")
        expect(assigns(:posts)).to eq([@post])
        expect(assigns[:posts].size).to eq 1
        expect(response.request.path_info).to eql("/topics/" + @topic.id.to_s + "/posts")
        expect(response).to be_successful
      end
      it "returns the post in between the dates" do
        post1 = Post.create!(name: "gana srinath", description: "123456789012345678901234567890asdd", user_id: @user.id, created_at: Time.new(1995, 06, 16, 0, 19), topic_id: @topic.id)
        get :index, params: {:topic_id => @topic.id, :start_date => "1995-06-15", :end_date => "1995-06-17"}
        expect(response).to render_template("index")
        expect(assigns[:posts]).to eq([post1])
        expect(assigns[:posts].size).to eq 1
        expect(response).to be_successful
      end
      it " should not returns the post in between the dates" do
        get :index, params: {topic_id: @topic.id, :start_date => "1995-06-17", :end_date => "1995-06-15"}
        expect(response).to_not be_successful
        expect(response.code).to eq("302")
        expect(flash[:alert]).to eq "date is out of range"
      end

      it 'invalid sign in' do
        sign_out @user
        get :index
        expect(response.code).to eq("302")
        expect(response).should redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
      end

    end

    context "pagination" do
      before do
        @post5 = []
        1.upto(7) do |i|
          @post5[i] = Post.create(name: "gakdkdna" + i.to_s, description: "123456789012345678901234567890", user_id: @user.id, topic_id: @topic.id, created_at: Date.yesterday)
        end
      end
      it "return the posts as per the page" do
        get :index, params: {:page => '1'}
        expect(response).to render_template("index")
        expect(assigns[:posts].size).to eq 6
        expect(response).to be_successful
      end
      it "return the posts as per the page" do
        get :index, params: {:page => '2'}
        expect(response).to render_template("index")
        expect(assigns[:posts].map(&:id).size).to eq 2
        expect(response).to be_successful
      end
    end
  end

  describe "GET #show" do
    before do
      @user3 = User.create(email: "3@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
      @rate1 = @post.ratings.create(rate: 2, post_id: @post.id, user_id: @user3.id)
      @rate2 = @post.ratings.create(rate: 4, post_id: @post.id, user_id: @user2.id)
      @comment = @post.comments.create(name: "gana", body: "hiii", user_id: @user.id)
      @tag = @post.tags.create(tagname: "tagname")
    end
    it "check the record not found" do
      get :show, params: {id: '205154841845487885178', topic_id: @topic.id}
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(topic_posts_path)
      expect(flash[:alert]).to eq "The Post you were looking for could not be found."
    end

    it "check render show page" do
      get :show, params: {id: @post.to_param, topic_id: @topic.id}
      expect(response).to render_template("show")
      expect((assigns(:post).tags.last).tagname).to eq("tagname")
      expect((assigns(:post).tags).size).to eq(2)
      expect(assigns(:post).name).to eq("gana")
      expect(assigns(:post).description).to eq("123456789012345678901234567890")
      expect(response).to be_successful
    end
    it "check comment count" do
      get :show, params: {id: @post.to_param, topic_id: @topic.id}
      expect(assigns(:post).comments_count).to eq 1
      expect(assigns(:post).comments[0].name).to eq("gana")
      expect(assigns(:post).comments[0].body).to eq("hiii")
      expect(response).to be_successful
    end
    it "check the avg ratings" do
      @avg_rate = @post.ratings.average(:rate).round(2)
      get :show, params: {id: @post.to_param, topic_id: @topic.id}
      expect(assigns[:rate_avg]).to eq(@avg_rate)
      expect(response).to render_template("show")
      expect(response).to be_successful
    end

    it "check the rate count group_by" do
      @count_rate = @post.ratings.group_by(&:rate)
      @raticount = Hash.new
      @raticount = {1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0}
      @output=@raticount.merge(@count_rate) {|key, val1, val2| val1 + val2.count}
      get :show, params: {id: @post.to_param, topic_id: @topic.id}
      expect(assigns[:rating_count_groupby]).to eq(@count_rate)
      expect(assigns(:result1)).to eq(@output)
      expect(response).to render_template("show")
      expect(response).to be_successful
    end
  end

  describe "GET edit" do
    it "get edit params" do
      get :edit, params: {id: @post.to_param, topic_id: @topic.id}
      expect(assigns(:post).name).to eq("gana")
      expect(assigns(:post).description).to eq("123456789012345678901234567890")
      expect(response).to be_successful
    end
    it "GET #edit by other user" do
      sign_out @user
      sign_in @user2
      get :edit, params: {id: @post.to_param, topic_id: @topic.id}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(response).to_not be_successful
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end
  end
  describe "POST #create" do
    context "with valid params" do
      it "creates a new Post" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "title", description: "123456789098765432134567890"}, tag_ids: [1]}
        }.to change(Post, :count).by(1)
        expect(response).to redirect_to(topic_posts_path)
        expect(flash[:notice]).to eq "Post has been created"
      end
      it "creates a new content" do
        expect {
          post :create, params: {topic_id: @topic.id, post: {name: "post", description: '12365489654136548965412354894120body2', tags_attributes: [tagname: "tag"]}}
        }.to change(Tag, :count).by(1)
        post = Post.last
        tag = post.tags.last
        expect(tag.tagname).to eql("tag")
      end
      it "creates a multiple new tag" do
        @post1 = @topic.posts.create(name: "post1", description: 'body123654789654122354789541585451', user_id: @user.id)
        @tag1 = Tag.create(id: 1, tagname: "tag1")
        @tag2 = Tag.create(id: 2, tagname: "tag2")
        expect {
          post :create, xhr: true, params: {topic_id: @topic.id, post: {name: "post2", description: 'b1254854124785258282525825845ody2', tag_ids: [@tag1.id, @tag2.id], tags_attributes: [tagname: "tag"]}}
        }.to change(Tag, :count).by(1)
        post = Post.last
        tag1 = post.tags.first
        tag2 = post.tags.second
        tag = post.tags.last
        expect(tag1.tagname).to eql("tag1")
        expect(tag2.tagname).to eql("tag2")
        expect(tag.tagname).to eql("tag")
      end
      it "as image file" do
        expect {
          post :create, xhr: true, params: {topic_id: @topic.id, post: {name: "content", description: "qwertyuiplkjhgfdsazxcvbnmbody", image: fixture_file_upload('/images/large/rails.jpg', 'image/jpg')}}
        }.to change(Post, :count).by(1)
        expect(assigns(:posts).count).to eq(2)
        expect(assigns(:post).image_file_name).to eq("rails.jpg")
      end
      it "as text file" do
        expect {
          post :create, xhr: true, params: {topic_id: @topic.id, post: {name: "content", description: "qwertyuiplkjhgfdsazxcvbnmbody", image: fixture_file_upload('/images/large/1.txt', 'text/txt')}}
        }.to change(Post, :count).by(0)
      expect(assigns(:post).errors[:image][1]).to eq("Please Upload valid images")

      end

      it "ajax creation form successful" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "title", description: "12345678901234567890123456789`"}}, format: :js}.to change(Post, :count).by(1)
        expect(response).to be_successful
      end
    end
    context "with invalid params new post" do
      it "with title nil" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "", description: "123456789012345678901234567890"}}
        }.to change(Post, :count).by(0)
        expect(response).to render_template("new")
        expect(assigns(:post).errors[:name][0]).to eq("can't be blank")
      end

      it "with body nil" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "title", description: "", tag_attributes: {tagname: 'new_tag'}}}
        }.to change(Post, :count).by(0)
        expect(response).to render_template("new")
        expect(assigns(:post).errors[:description][0]).to eq("can't be blank")
      end
      it "ajax title with more than 20 char" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "1234567890123456789012", description: "body",
                                                                   tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        expect(assigns(:post).errors[:name][0]).to eq("is too long (maximum is 20 characters)")
      end
      it "ajax description with less  than 25 char" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "1234", description: "body", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        expect(assigns(:post).errors[:description][0]).to eq("is too short (minimum is 25 characters)")
      end

      it "ajax creation form failed" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "", description: "", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        expect(assigns(:post).errors[:name][0]).to eq("can't be blank")
        expect(assigns(:post).errors[:description][0]).to eq("can't be blank")
        expect(response).to be_successful
      end
    end
  end
  describe "Read status" do
    context "with valid params" do
      it "enter the post_id and user_id" do
        get :post_user_status, params: {id: @post.id, topic_id: @topic.id}
        expect(response.request.path_info).to eql("/topics/" + @topic.id.to_s + "/posts/" + @post.id.to_s + "/post_user_status")
        expect(assigns(:post).users.last.id).to eql(@user.id)
      end
    end
  end
  describe "PUT #update" do
    context "with valid params" do
      it "updates the requested post" do
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {name: "update title", description: "1234567890-09876543211234567", tag_attributes: {tagname: 'new_tag'}}}
        expect(assigns(:post).name).to eq("update title")
        expect(flash[:notice]).to eq "Post has been updated"
      end

      it "update requested post by other user" do
        sign_out @user
        sign_in @user2
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {name: "update title", description: "updatebodykvmkvmkvmkvmmvkmvkmvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv", tag_attributes: {tagname: 'new_tag'}}}
        expect(flash[:alert]).to eq "You are not authorized to access this page."
      end
      it "updates the requested user" do
        @tag = @post.tags.create(tagname: "tag1")
        put :update, params: {topic_id: @topic.id, id: @post.to_param, post: {content: "post2", tags_attributes: [tagname: "tag"]}}
        @post.reload
        post = Post.last
        tag = post.tags.last
        expect(tag.tagname).to eql("tag")
        expect(assigns(:post).tags.last.tagname).to eql("tag")
      end

      it "redirects to the post" do
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {name: "update title", description: "updatejffffffffffffffffffffffffffffffffffffffffffffffffffffffffbody", tag_attributes: {tagname: 'new_tag'}}}
        expect(assigns(:post).name).to eq("update title")
        expect(assigns(:post).description).to eq("updatejffffffffffffffffffffffffffffffffffffffffffffffffffffffffbody")
        expect(flash[:notice]).to eq "Post has been updated"
      end
      it "ajax updation form successful" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "updatetitle", description: "up12345678901234567890123456789", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(1)
        expect(assigns(:post).name).to eq("updatetitle")
        expect(assigns(:post).description).to eq("up12345678901234567890123456789")
        expect(response).to be_successful
      end
    end
    context "with invalid params" do
      it "returns a success response (i.e. to display the 'edit' template)" do
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {name: "", description: "", tag_attributes: {tagname: 'new_tag'}}}
        assigns(:post).errors.empty?.should_not be true
      end

      it "updates the requested post with invalid title" do
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {name: "", description: "1234567890987654321234567890", tag_attributes: {tagname: 'new_tag'}}}
        expect(assigns(:post).errors).to_not be_empty
        expect(assigns(:post).errors[:name][0]).to eq("can't be blank")
      end

      it "updates the requested post with invalid body" do
        put :update, params: {id: @post.to_param, topic_id: @topic.id, post: {title: "update title", description: "", tag_attributes: {tagname: 'new_tag'}}}
        expect(assigns(:post).errors[:description][0]).to eq("can't be blank")
      end
      it "ajax title with more than 20 char" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "1234567890123456789012", description: "body", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        expect(assigns(:post).errors[:name][0]).to eq("is too long (maximum is 20 characters)")
      end
      it "ajax description with less  than 25 char" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "1234", description: "body", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        expect(assigns(:post).errors[:description][0]).to eq("is too short (minimum is 25 characters)")
      end

      it "ajax update form failed" do
        expect {post :create, params: {topic_id: @topic.id, post: {name: "", description: "", tag_attributes: {tagname: 'new_tag'}}}, format: :js}.to change(Post, :count).by(0)
        assigns(:post).errors.empty?.should_not be true
        expect(response).to be_successful
      end
    end

  end
  describe "DELETE #destroy" do
    it "destroys the requested post" do
      delete :destroy, params: {id: @post.to_param, topic_id: @topic.id}
      expect(Post.count).to eq(0)
      expect(flash[:alert]).to eq "Post was successfully destroyed."
    end

    it "destroys the request by other user" do
      sign_out @user
      sign_in @user2
      delete :destroy, params: {id: @post.to_param, topic_id: @topic.id}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end
  end
end
