require 'rails_helper'

RSpec.describe RatingsController, type: :controller do
  before {
    @user = User.create!(email: "email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    sign_in @user
    @user2 = User.create!(email: "2email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    @topic = @user.topics.create!(name: "anything", user_id: 1)
    @post = @topic.posts.create!(name: "gana", description: "1234567890987654321234567890", user_id: @user.id)
    @rating = @post.ratings.create!(rate: 5, post_id: @post.id)
  }
  describe "GET #index" do
    it "returns a success response with post id" do
      get :index, params: {id: @rating.to_param, post_id: @post.id, topic_id: @topic.id, user_id: @user.id}
      expect(response).to render_template("index")
      expect(response.request.path_info).to eql("/topics/" + @topic.id.to_s + "/posts/" + @post.id.to_s + "/ratings")
      expect(assigns(:ratings)).to eq([@rating])
      expect(assigns[:ratings].size).to eq 1
      expect(response).to be_successful
    end
    it 'invalid sign in' do
      sign_out @user
      get :index, params: {id: @rating.to_param, post_id: @post.id, topic_id: @topic.id, user_id: @user.id}
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
  end

  describe "GET #show" do
    it "check the record not found" do
      get :show, params: {id: '205154841845487885178', topic_id: @topic.id, post_id: @post.id}
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(topic_post_path(@topic,@post))
      expect(flash[:alert]).to eq "The Rating you were looking for could not be found."

    end
    it "returns http success" do
      get :show, params: {id: @rating.to_param, post_id: @post.id, topic_id: @topic.id, user_id: @user.id}
      expect(assigns(:rating).rate).to eq(5)
      expect(response).to be_successful
    end
  end
  describe 'GET #new' do
    it 'returns success ' do
      get :new, params: { id: @rating.to_param, post_id: @post.id, topic_id: @topic.id }
      expect(assigns(:rating)).to be_a_new(Rating)
      expect(response).to be_successful
    end
    it "returns a success response" do
      sign_out @user
      get :new,  params: { id: @rating.to_param, post_id: @post.id, topic_id: @topic.id }
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
  end

  describe 'get #create ' do
    it "does creates a new rate" do
      expect {
        post :create, params: { rating: { rate: '' }, id: @rating.id, post_id: @post.id, topic_id: @topic.id }
      }.to change(Rating, :count).by(0)
      expect(assigns(:rating).errors[:rate][0]).to eq("can't be blank")
    end
    it 'creates a new rating' do
      expect do
        post :create, params: { rating: { rate: 'anything' }, id: @rating.id, post_id: @post.id, topic_id: @topic.id }
      end.to change(Rating, :count).by(1)
      expect(flash[:notice]).to eq 'rating has been done'
      expect(response).to redirect_to topic_post_path(@topic.id, @post.id)
    end
    it "returns a success response" do
      sign_out @user
      get :show, params: { rating: { rate: 'anything' }, id: @rating.id, post_id: @post.id, topic_id: @topic.id }
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
end

end
