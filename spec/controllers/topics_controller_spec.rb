require 'rails_helper'

RSpec.describe TopicsController, type: :controller do
  before do
    @user = User.create(email: "email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    sign_in @user
    @user2 = User.create(email: "2email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    @topic = @user.topics.create(name: "anything")
  end
  describe "GET #index" do
    it "it has index " do
      get :index, params: {id: @topic.to_param, user_id: @user.id}
      expect(response.request.path_info).to eql("/topics")
      expect(response).to render_template("index")
      expect(assigns(:topics)).to eq([@topic])
      expect(assigns[:topics].size).to eq 1
      expect(response.status).to eql(200)
    end
    it 'invalid sign in' do
      sign_out @user
      get :index
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end
    context "pagination" do
      before do
        @topic7=[]
        1.upto(8) do |i|
          @topic7[i]=Topic.create!(name: "topic"+i.to_s, user_id: @user.id)
        end
      end
      it "should have content in page 1" do
      get :index, params: {:page => '1'}
      expect(response).to render_template("index")
      expect(assigns[:topics].size).to eq(8)
      expect(response).to be_successful
      end
      it "should have content in page 2" do
        get :index, params: {:page => '2'}
        expect(response).to render_template("index")
        expect(assigns[:topics].map(&:id).size).to eq(1)
        expect(response).to be_successful
      end
    end
    context 'with json request auth failed' do
      it 'returns a json response page1' do
        sign_out @user
        get :index, params: {}, format: :json
        expect(response.status).to eql 401
      end
    end

    context 'with json request auth passed' do
      before do
        request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('email@email.com', "Srinath171@")
      end
      it 'returns a json response page1' do
        get :index, params: {:page=> '1'}, format: :json
        expect(response.status).to eql 200
      end
      it 'returns a json response page2' do
        get :index, params: {
            page: 2
        }, format: :json
        expect(response.status).to eql 200
      end

    end
    context 'devise' do
      it 'should detect the device' do
        request.env['HTTP_USER_AGENT'] =  'windows'
        get :devise
        expect(response.request.path_info).to eql("/devise")
        expect(response).to redirect_to(/\.microsoft\.com/)
        expect(response.code).to eq("302")
      end
      it 'should detect the device' do
        request.env['HTTP_USER_AGENT'] =  'mac'
        get :devise
        expect(response.request.path_info).to eql("/devise")
        expect(response).to redirect_to('https://itunes.apple.com/us/app/apple-store/id375380948?mt=8')
        expect(response.code).to eq("302")
      end
      it 'should detect the device' do
        request.env['HTTP_USER_AGENT'] =  'android'
        get :devise
        expect(response.request.path_info).to eql("/devise")
        expect(response).to redirect_to('https://play.google.com/store?hl=en')
        expect(response.code).to eq("302")
      end
      it 'should detect the device' do
        request.env['HTTP_USER_AGENT'] =  'annjdhd'
        get :devise
        expect(response.request.path_info).to eql("/devise")
        expect(response).to redirect_to(root_path)
        expect(response.code).to eq("302")
      end
      it 'invalid sign in' do
        sign_out @user
        get :devise
        expect(response).to_not be_successful
        expect(response.code).to eq("302")
        expect(response).should redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
      end
    end
end
  describe "GET #show" do
    it "check the record not found" do
      get :show, params: {id: '205154841845487885178'}
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(topics_path)
      expect(flash[:alert]).to eq "The Topic you were looking for could not be found."

    end
    it "returns show" do
      get :show, params: {id: @topic.to_param}
      expect(response).to render_template("show")
      expect(assigns(:topic).name).to eq("anything")
      expect(response).to be_successful
    end
    context 'with json request auth failed' do
      before do
        sign_out @user
      end

      it 'returns a json response' do
        get :show, params: { id: @topic.to_param }, format: :json
        expect(response.status).to eql 401
      end
    end
    context 'with json request auth passed' do
      before do
        request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('email@email.com', "Srinath171@")
      end

      it 'returns a json response' do
        get :show, params: { id: @topic.to_param }, format: :json
        expect(response.status).to eql 200
      end
    end


  end

  describe "GET edit" do
    it "get edit params" do
      get :edit, params: {id: @topic.to_param}
      expect(assigns(:topic).name).to eq("anything")
      expect(response).to be_successful
    end
    it 'invalid sign in' do
      sign_out @user
      sign_in @user2
      get :edit, params: {id: @topic.to_param}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(response).to_not be_successful
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Topic" do
        expect {post :create, params: { topic: {name: "title"}}}.to change(Topic, :count).by(1)
        expect(response).to redirect_to(topics_path)
        expect(flash[:notice]).to eq "Topic has been created"
      end
      it "ajax creation from successful" do
        expect {post :create, params: { topic: {name: "title"}}, format: :js}.to change(Topic, :count).by(1)
        expect(response).to be_successful
      end
    end
    context "with invalid params new topic" do
      it "with name nil" do
        expect {post :create, params: { topic: {name: ""}}}.to change(Topic, :count).by(0)
        expect(response).to render_template("new")
        expect(assigns(:topic).errors[:name][0]).to eq("can't be blank")
      end
      it " name with more than 20 char" do
        expect {post :create, params: { topic: {name: "1234567890123456789012"}}}.to change(Topic, :count).by(0)
        expect(assigns(:topic).errors[:name][0]).to eq("is too long (maximum is 20 characters)")
        expect(response).to render_template("new")
      end
      it "ajax creation form failed" do
        expect {post :create, params: { topic: {name: "1234567890123456789012"}}, format: :js}.to change(Topic, :count).by(0)
        expect(assigns(:topic).errors[:name][0]).to eq("is too long (maximum is 20 characters)")
      end
    end

    context 'create json api call auth failed' do
      before do
        sign_out @user
      end

      it 'returns a json response' do
        post :create, params: { topic: { name: 'new' } }, format: :json
        expect(response.status).to eql 401
      end
    end
    context 'create json api call auth passed' do
      before do
        request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('email@email.com', "Srinath171@")
      end

      it 'returns a json response' do
        post :create, params: { topic: { name: 'new' } }, format: :json
        expect(response.status).to eql 201
      end

      it 'returns a invalid json response' do
        post :create, params: { topic: { name: '' } }, format: :json
        expect(JSON.parse(response.body)["name"][0]).to  eq("can't be blank")
        expect(response.status).to eql 422
      end
    end

  end
  describe "PUT #update" do
    context "With valid params" do
      it "updates the requested topic" do
        patch :update, params: {id: @topic.to_param, topic: {name: "update name"}}
        expect(assigns(:topic).name).to eq("update name")
        expect(flash[:notice]).to eq "Topic was successfully updated"
      end
      it "updates the ajax requested topic" do
        patch :create, params: {id: @topic.to_param, topic: {name: "update name"}, format: :js}
        expect(assigns(:topic).name).to eq("update name")
        expect(response).to be_successful
      end
      it "update requested topic by other user" do
        sign_in @user2
        patch :update, params: {id: @topic.to_param, topic: {name: "update name"}}
        expect(flash[:alert]).to eq "You are not authorized to access this page."
      end
    end
    context "With invalid params" do
      it "updates the topic with null" do
        patch :update, params: {id: @topic.to_param, topic: {name: ""}, format: :js}
        expect(assigns(:topic).errors[:name][0]).to eq("can't be blank")
      end
      it "updates the topic with invalid name" do
        patch :update, params: {id: @topic.to_param, topic: {name: "jbdjdbiubdibdbudbiubdubdubdu"}, format: :js}
        expect(assigns(:topic).errors[:name][0]).to eq("is too long (maximum is 20 characters)")
      end
    end
  end
  describe "DELETE #destroy" do
    it "it destroy the requested topic" do
      delete :destroy, params: {id: @topic.to_param}
      expect(Topic.count).to eq(0)
      expect(flash[:alert]).to eq "Topic was successfully destroyed."
    end
    it "destroys the request by other user" do
      sign_out @user
      sign_in @user2
      delete :destroy, params: {id: @topic.to_param}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end

  end
end
