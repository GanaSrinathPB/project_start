require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  before  do
    @user = User.create!(email: 'email@email.com', password:"Srinath171@", password_confirmation:"Srinath171@")
  end
  describe 'GET #index' do
    before  do
      request.env['HTTP_AUTHORIZATION'] = ActionController::HttpAuthentication::Basic.encode_credentials('email@email.com', "Srinath171@")
    end
    it 'user and valid password are present' do
      get :index, params: {}, format: :json
      expect(@user).to be_valid
      expect(response.status).to eql 200
    end

    it 'user and valid password are absent' do
      @user.email = nil
      expect(@user).to_not be_valid
      expect(response.status).to eql 200
    end
  end

end
