require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  before do
    @user = User.create!(email: "email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    sign_in @user
    @user2 = User.create!(email: "2email@gmail.com", password: "Srinath171@", password_confirmation: "Srinath171@")
    @topic = @user.topics.create!(name: "anything", user_id: 1)
    @post = @topic.posts.create!(name: "gana", description: "1234567890123456789009876543", user_id: @user.id)
    @comment = @post.comments.create!(name: "email@gamil.com", body: "comments", user_id: @user.id)
    @user_rate= @comment.user_comment_ratings.create(rate:4, user_id:@user.id)
  end
  describe "GET #index" do
    context "with html request" do
      it "returns a success response with post id" do
        get :index, params: {post_id: @post.id, topic_id: @topic.id}
        expect(response).to render_template("index")
        expect(response.request.path_info).to eql("/topics/" + @topic.id.to_s + "/posts/" + @post.id.to_s + "/comments")
        expect(assigns(:comments)).to eq([@comment])
        expect(assigns[:comments].size).to eq 1
        expect(response).to be_successful
      end

      it 'invalid sign in' do
        sign_out @user
        get :index, params: {post_id: @post.id, topic_id: @topic.id}
        expect(response).to_not be_successful
        expect(response.code).to eq("302")
        expect(response).should redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
      end
    end
  end
  describe "GET #show" do
    it "check the record not found" do
      get :show, params: {id: '205154841845487885178', topic_id: @topic.id, post_id: @post.id}
      expect(response).to_not be_successful
      expect(response.code).to eq("302")
      expect(response).should redirect_to(topic_post_path(@topic,@post))
      expect(flash[:alert]).to eq "The Record you were looking for could not be found."

    end
    it "returns a success response" do
      get :show, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(assigns(:comment).name).to eq("email@gamil.com")
      expect(assigns(:comment).body).to eq("comments")
      expect(response).to be_successful
    end
  end

  describe "GET #edit" do
    it "returns a success response" do
      get :edit, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(assigns(:comment).name).to eq("email@gamil.com")
      expect(assigns(:comment).body).to eq("comments")
      expect(response).to be_successful
    end
    it "GET #edit by other user" do
      sign_out @user
      sign_in @user2
      get :edit, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(response).to_not be_successful
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end
  end

  describe "GET #new" do
    context 'with html request' do
      it 'returns sucess ' do
        get :new, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
        expect(assigns(:comment)).to be_a_new(Comment)
        expect(response).to be_successful
      end

      it "returns a success response" do
        sign_out @user
        get :new, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
        expect(response).to redirect_to(new_user_session_path)
        expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
      end
    end
  end
  describe "POST #create" do
    context "with valid params" do
      it "creates a new comment" do
        expect {
          post :create, :params => {:comment => {name: "anything", body: "anything"}, id: @comment.id, post_id: @post.id, topic_id: @topic.id}
        }.to change(Comment, :count).by(1)
        expect(flash[:notice]).to eq "Comment was successfully created."
        expect(response).to redirect_to topic_post_path(@topic.id, @post.id)
      end
      it "does creates a new comment" do
        expect {
          post :create, :params => {:comment => {name: "", body: ""}, id: @comment.id, post_id: @post.id, topic_id: @topic.id}
        }.to change(Comment, :count).by(0)
        expect(assigns(:comment).errors[:name][0]).to eq("can't be blank")
        expect(assigns(:comment).errors[:body][0]).to eq("can't be blank")
      end
    end
  end
  describe "user comment rate" do
    context "rate the comment" do
      it "create user rate" do
        expect{
          post :create, :params=>{:user_comment_ratings=>{rate:4,comment_id:@comment.id,user_id:@user.id}
          }
          expect(flash[:notice]).to eq "successfully updated comment's rating by user"
          expect(response).to redirect_to topic_post_path(@topic.id, @post.id)
        }
      end
      it " should not create rate "do
        expect{
          post :create, :params=>{:user_comment_ratings=>{rate:4,comment_id:@comment.id,user_id:@user.id}
          }
          expect(flash[:alert]).to eq "You have already rated the comment"
          expect(response).to redirect_to topic_post_path(@topic.id, @post.id)
        }
      end
    end

  end
  describe "comment rate show" do
    it "should display the comment ratings"do
      get :comment_rate_show ,:params=>{id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(assigns(:rate_comment).count).to eq(1)
      expect(assigns(:rate_comment).last.user.email).to eq(@user.email)
      expect(assigns(:rate_comment).last.rate).to eq(4)
    end
  end
  describe "PUT #update" do
    it "updates the requested comments" do
      put :update, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id, comment: {name: "updateanything", body: "anything"}}
      expect(assigns(:comment).name).to eq("updateanything")
      expect(assigns(:comment).body).to eq("anything")
      expect(flash[:notice]).to eq "Comment was successfully updated."
    end

    it "returns a success response" do
      sign_out @user
      get :show, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end

  end

  describe "DELETE #destroy" do
    it "destroys the requested post" do
      delete :destroy, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(Comment.count).to eq(0)
      expect(flash[:alert]).to eq 'Comment was successfully destroyed.'
    end
    it "destroys the request by other user" do
      sign_out @user
      sign_in @user2
      delete :destroy, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(response.code).to eq("302")
      expect(response).should redirect_to(root_path)
      expect(flash[:alert]).to eq "You are not authorized to access this page."
    end
    it "returns a success response" do
      sign_out @user
      get :show, params: {id: @comment.to_param, post_id: @post.id, topic_id: @topic.id}
      expect(response).to redirect_to(new_user_session_path)
      expect(flash[:alert]).to eq "You need to sign in or sign up before continuing."
    end

  end

end

