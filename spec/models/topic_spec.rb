require 'rails_helper'

RSpec.describe Topic, type: :model do
  subject {
    described_class.new(email: "1@gmail.com", encrypted_password: "password")
  }
  subject {
    described_class.new(name: "Sports", user_id: 1)
  }
  describe "validation" do

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end
    it { should validate_uniqueness_of(:name).case_insensitive }
    it {should validate_length_of(:name).is_at_most(20)}
  end

  describe "association" do
    it { should have_many(:posts).dependent(:destroy)}
    it { Topic.reflect_on_association(:user).macro.should   eq(:belongs_to) }

  end
end
