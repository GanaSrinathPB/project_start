require 'rails_helper'

RSpec.describe Rating, type: :model do
  let(:user) {User.new(id: 1, email: "1@gmail.com", encrypted_password: "password")}
  let(:topic) {Topic.new(id: 2, name: "Sports", user_id: user.id)}
  let(:post) {Post.new(id: 3,name: "Cricket", description: "anything", topic_id: topic.id, user_id: user.id )}
  subject{
    described_class.new(rate: 5 , post_id: post.id)
  }

  describe "Association" do
    it{ should belong_to(:post)}
  end
end
