require 'rails_helper'

RSpec.describe Post, type: :model do
  let(:user) {User.new(id: 1, email: "1@gmail.com", encrypted_password: "Password@123")}
  let(:topic) {Topic.new(id: 2, name: "Sports", user_id: 1)}
  subject {
    described_class.new(name: "Cricket", description: "12345678912345678123457874185963", topic_id: topic.id, user_id: user.id)
  }
  describe "validation" do
    it "is not valid without valid attributes" do
      subject.name = nil
      subject.description = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without a description" do
      subject.description = nil
      expect(subject).to_not be_valid
    end
    it {should validate_length_of(:name).is_at_most(20)}
      it { should validate_uniqueness_of(:name).case_insensitive}
    it {should validate_length_of(:description).is_at_least(25)}
    it { should have_attached_file(:image) }
    it { should validate_attachment_content_type(:image).
        allowing('image/png').
        allowing('image/gif').
        allowing('image/jpeg').
        rejecting('text/plain', 'text/xml')
    }

  end

  describe "association" do

    it {Post.reflect_on_association(:user).macro.should eq(:belongs_to)}
    it {Post.reflect_on_association(:topic).macro.should eq(:belongs_to)}
    it {Post.reflect_on_association(:comments).macro.should eq(:has_many)}
    it {Post.reflect_on_association(:ratings).macro.should eq(:has_many)}
    it {should  have_and_belong_to_many(:users)}
    it { should have_many(:tags).through(:taggings)}
  end
  it do
    should accept_nested_attributes_for(:tags).
        allow_destroy(true)
  end
end
