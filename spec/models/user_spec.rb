require 'rails_helper'

RSpec.describe User, type: :model do
  subject {described_class.new(email: "34@gmail.com", password: "Srinath17@")}
  describe "validation " do
    it "is valid with email and password" do
      expect(subject).to be_valid
    end
    it "is not valid without email" do
      subject.email = nil
      expect(subject).to_not be_valid
    end
    it "is not valid without password" do
      subject.password = nil
      expect(subject).to_not be_valid
    end
    it { should validate_uniqueness_of(:email).ignoring_case_sensitivity }
  end

  describe "association" do
    it {User.reflect_on_association(:posts).macro.should eq(:has_many)}
    it {User.reflect_on_association(:topics).macro.should eq(:has_many)}
    it {User.reflect_on_association(:comments).macro.should eq(:has_many)}
    it do
      should have_and_belong_to_many(:post_reads).
          class_name('Post')
    end
    it { should have_many(:user_comment_ratings)}
    it do
       should have_many(:rate_comments).through(:user_comment_ratings).class_name('Comment').dependent(:destroy)
    end
  end
end
