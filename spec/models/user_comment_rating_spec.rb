require 'rails_helper'

RSpec.describe UserCommentRating, type: :model do
describe "Validation" do
  it { should validate_presence_of(:rate) }
end
describe "Association" do
  it { should belong_to(:user) }
  it { should belong_to(:comment) }
end
end
