require 'rails_helper'

RSpec.describe Comment, type: :model do
  let(:user) {User.new(id: 1, email: "1@gmail.com", encrypted_password: "password")}
  let(:topic) {Topic.new(id: 2, name: "Sports", user_id: user.id)}
  let(:post) {Post.new(id: 3,name: "Cricket", description: "anything", topic_id: topic.id, user_id: user.id )}
  subject{
    described_class.new(name: "pbgsrinath@gmail.com", body: "comment-body", post_id: post.id, user_id: user.id)
  }

  describe "validation" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:body) }
  end
  describe "association" do
    it { should belong_to(:post).counter_cache(true) }
    it { should belong_to(:user) }
    it { should have_many(:user_comment_ratings)}
    it { should have_many(:users).through(:user_comment_ratings).dependent(:destroy)}
  end
end
