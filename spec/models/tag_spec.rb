require 'rails_helper'

RSpec.describe Tag, type: :model do
  subject{
    described_class.new(tagname: "hiiiii")
  }
  describe "validation" do
    it { should validate_uniqueness_of(:tagname).case_insensitive }
  end

  describe  "association" do
    it { should have_many(:taggings)}
    it { should have_many(:posts).through(:taggings)}
  end
end

