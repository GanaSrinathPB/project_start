require "rails_helper"

RSpec.describe TopicMailer, type: :mailer do
  before do
    @user=User.create(email:"pbgsrinath@gmail.com",password:"17111997Gana@", password_confirmation:"17111997Gana@")
    @topic=Topic.create(name:"name",user_id:@user.id)
  end
    describe "support" do

      let(:email) { TopicMailer.welcome_topic(@user.id,@topic.id) }

      it "renders the subject" do
        expect(email.subject).to eq 'Welcome to My Awesome Site'
      end

      it "renders the receiver email" do
        expect(email.to).to eql [@user.email]
      end

      it "renders the sender email" do
        expect(email.from).to eq ["srigana1997@gmail.com"]
      end


    end

end

# describe 'create topic' do
#   let(:mail) { UserMailer.create_topic(@user.id) }
#
#   it 'renders the headers' do
#     expect(mail.subject).to eq('topic created successfully')
#     expect(mail.to).to eq(['user@gmail.com'])
#     expect(mail.from).to eq(['simbu@gmail.com'])
#   end
# end

