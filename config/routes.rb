Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web, at: '/sidekiq'
  devise_for :users
  resources :tags
  resources :topics do
    resources :posts do
      get :post_user_status, on: :member
      resources :comments do
        member do
          get :comment_rate_show
          post :user_comment_rate
        end
      end
      resources :ratings
    end
  end
  get :devise, to: 'topics#devise'
  get '/users', to: 'users#index', :defaults => {:format => 'json'}
  resources :posts, :only => :index
  root "topics#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
