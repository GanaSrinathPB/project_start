Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.
  Paperclip.options[:command_path] = "C:\Program Files (x86)\GnuWin32\bin" #paperclip
  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
        'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = true

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true
  config.action_mailer.default_url_options = {host: 'localhost', port: 3000}

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  # config.file_watcher = ActiveSupport::EventedFileUpdateChecker
  # config.action_mailer.perform_deliveries = true
  #letter opener
  config.action_mailer.delivery_method = :letter_opener
  # config.action_mailer.delivery_method = :smtp
  #Defult Smtp
    # config.action_mailer.smtp_settings = {
    #     address:              'smtp.gmail.com',
    #     port:                 587,
    #     domain:               'example.com',
    #     user_name:            'srigana1997@gmail.com',
    #     password:             '17111997gana',
    #     authentication:       'plain',
    #     enable_starttls_auto: true
    # }
  # # mail gun Smtp
  # config.action_mailer.smtp_settings = {
  #     address:              'smtp.mailgun.org',
  #     port:                 587,
  #     domain:               'sandboxc46154c216f442f9abd77241d42fd5f2.mailgun.org',
  #     user_name:            'postmaster@sandboxc46154c216f442f9abd77241d42fd5f2.mailgun.org',
  #     password:             '4c2eed853dc18d2202ed8bf0d5f07a9a-2b778fc3-0131e44b',
  #     authentication:       'plain'
  # }

end
